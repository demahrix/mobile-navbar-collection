import 'package:flutter/material.dart';

class MobileNavbar extends StatelessWidget {

  final int selectedIndex;
  final ValueChanged<int> onChanged;
  final List<IconData> items;
  final double height;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? margin;
  final Color? backgroundColor;
  final Color? unselectedColor;
  final Color? selectedColor;
  final double iconSize;
  final double selectedIconSize;
  final BorderRadiusGeometry? borderRadius;
  final ShapeBorder? shape;

  const MobileNavbar({
    Key? key,
    this.selectedIndex = 0,
    required this.onChanged,
    required this.items,
    this.height = kBottomNavigationBarHeight,
    this.padding,
    this.margin,
    this.backgroundColor,
    this.unselectedColor,
    this.selectedColor,
    this.iconSize = 24.0,
    this.selectedIconSize = 24.0,
    this.borderRadius,
    this.shape
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      padding: padding,
      margin: margin,
      decoration: shape != null
        ? ShapeDecoration(
            shape: shape!,
            color: backgroundColor
          )
        : BoxDecoration(
            borderRadius: borderRadius,
            color: backgroundColor
          ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: List.generate(items.length, (index) {

          bool selected = index == selectedIndex;

          return GestureDetector(
            onTap: () => onChanged(index),
            child: Icon(
              items[index],
              size: selected ? selectedIconSize : iconSize,
              color: selected ? selectedColor : unselectedColor,
            ),
          );
        }),
      ),
    );
  }

}
